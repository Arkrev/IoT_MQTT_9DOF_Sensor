﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Text;
using System;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;

using System;

public class mqttTest : MonoBehaviour
{


    private Vector3 orientation = Vector3.zero;
	private Vector3 offset = Vector3.zero;
	private bool offsetCalibrated = false;
	private bool offset_x = false;
	private bool offset_y = false;
	private bool offset_z = false;


    [SerializeField]
    private GameObject _3Dmodel;

	[SerializeField]
	private float turningRate;


    void Start()
    {

		MqttClient client = new MqttClient(IPAddress.Parse("192.168.4.2"));//"192.168.1.159"




        //listen:



        string clientId = Guid.NewGuid().ToString();
        client.Connect(clientId);

        // subscribe to the topic "/home/temperature" with QoS 2
        client.Subscribe(new string[] { "roll", "pitch", "yaw" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });


        client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;





        //publish:

        /*
		string strValue = Convert.ToString(84);

		// publish a message on "/home/temperature" topic with QoS 2
		client.Publish("hello/world", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE , true);
		*/
    }


    void client_MqttMsgSubscribed(object sender, MqttMsgSubscribedEventArgs e)
    {
        Debug.Log("Subscribed for id = " + e.MessageId);
    }
    void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
    {
        Debug.Log("Received = " + Encoding.UTF8.GetString(e.Message) + " on topic " + e.Topic);
		string message = Encoding.UTF8.GetString (e.Message);
		Debug.Log ("message = " + message);
		float angle = float.Parse(message,
			System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
		if (offsetCalibrated) {
		
			if (e.Topic == "roll") {
				orientation.x = angle - offset.x;
			} else if (e.Topic == "pitch") {
				orientation.y = angle - offset.y;
			} else if (e.Topic == "yaw") {
				orientation.z = angle - offset.z;
			} else {
				Debug.Log ("you shouldn't be there" + e);
			}
		} else {
			if (e.Topic == "roll") {
				offset.x = angle;
				offset_x = true;
			} else if (e.Topic == "pitch") {
				offset.y = angle;
				offset_y = true;
			} else if (e.Topic == "yaw") {
				offset.z = angle;
				offset_z = true;
			} else {
				Debug.Log ("you shouldn't be there" + e);
			}
			if (offset_x && offset_y && offset_z) {
				offsetCalibrated = true;
			}

		}
    }

    void Update()
    {
		_3Dmodel.transform.rotation = Quaternion.RotateTowards(_3Dmodel.transform.rotation, Quaternion.Euler(orientation), turningRate * Time.deltaTime);
		//_3Dmodel.transform.rotation = Quaternion.Euler(orientation);

    }

}
