﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Text;
using System;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;

using System;

public class mqttTestPrevious : MonoBehaviour {
	

	private Vector3 gyro = Vector3.zero;


	[SerializeField]
	private GameObject _3Dmodel;


	void Start(){

		MqttClient client = new MqttClient(IPAddress.Parse("10.42.0.1"));//"192.168.1.145"




		//listen:



		string clientId = Guid.NewGuid().ToString();
		client.Connect(clientId);

		// subscribe to the topic "/home/temperature" with QoS 2
		client.Subscribe(new string[] { "gyro_X","gyro_Y","gyro_Z" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE,MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE,MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });


		client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;





		//publish:

		/*
		string strValue = Convert.ToString(84);

		// publish a message on "/home/temperature" topic with QoS 2
		client.Publish("hello/world", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE , true);
		*/
	}


	void client_MqttMsgSubscribed(object sender, MqttMsgSubscribedEventArgs e)
	{
		Debug.Log("Subscribed for id = " + e.MessageId);
	}
	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
	{
		Debug.Log("Received = " + Encoding.UTF8.GetString(e.Message) + " on topic " + e.Topic);

		int angle = Int32.Parse(Encoding.UTF8.GetString (e.Message));

		if (e.Topic == "gyro_X") {
			gyro.x += angle;
		} else if (e.Topic == "gyro_Y") {
			gyro.y += angle;
		} else if (e.Topic == "gyro_Z") {
			gyro.z += angle;
		} else {
			Debug.Log ("you shouldn't be there" + e);
		}
	}

	void Update(){
		_3Dmodel.transform.rotation = Quaternion.Euler(gyro);
	}

}
