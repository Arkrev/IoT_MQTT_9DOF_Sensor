﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Text;
using System;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;


public class mqttTest : MonoBehaviour
{
    //Used to store euler data send by the sensor.
    private Vector3 Orientation;

    //offset Quaternion
    private Boolean changeOffsetQuat = false;
    private Quaternion offQuat = Quaternion.identity;

    //magnetic declination. BE CAREFUL, IT CHANGES OVER TIME.
    private float magDeclination = 0.49f;


    //Array used to check if a button changed state.
    private String[] buttonPreviousState = new String[8];
    private String[] buttonCurrentState = new String[8];

    //Quaternion used to rotate _3Dmodel.
    private Quaternion q = Quaternion.identity;

    //Vector data of Imu Acceleration, Magnetometer , Gyroscope
    private Vector3 imua = Vector3.zero;
    private Vector3 imum = Vector3.zero;
    private Vector3 imug = Vector3.zero;
    //Offset for each vector of data.
    //Equal to the first data received
    private Vector3 imuoffa = Vector3.zero;
    private Vector3 imuoffm = Vector3.zero;
    private Vector3 imuoffg = Vector3.zero;
    //variable used for stocking previous result.
    private static int maxSizeOfHistory = 500;
    private int historyIndex = 0;
    //counter for the init ( should go until maxSizeOfHistory)
    private int initCount = 0;

    //Vector array used to stock previous result of each Imu Vector.
    private Vector3[] prevImuA = new Vector3[maxSizeOfHistory];
    private Vector3[] prevImuM = new Vector3[maxSizeOfHistory];
    private Vector3[] prevImuG = new Vector3[maxSizeOfHistory];

    //Bool to check is the offset has been computed.
    private bool offsetted = false;

    //variable used by Madgwick in order to compute the quaternion from the IMU data.
    //FixedUPdate is called 50 time per seconds, and it is fixed update that call madgwick function
    private float sampleFreq = 50;

    //---------------------------------------------------------------------------------------------------
    // Variable definitions
    //arbitrary factor for Madgwick
    private float beta = 1.5f;//Arbitrary value, change the speed// 2 * proportional gain (Kp)
    //Default Quaternion variable for Madgwick
    private float q0 = 1.0f, q1 = 0.0f, q2 = 0.0f, q3 = 0.0f;   // quaternion of sensor frame relative to auxiliary frame

    //---------------------------------------------------------------------------------------------------
    // Function declarations
    //variable used for changing the displayed model.
    private int currentDisplayedChild = 0;
    private bool mustChangeModel = false;


    //current connexion state
    private bool connected;

    //calibration variable;
    private Vector3 magMax = Vector3.zero;
    private Vector3 magMin = Vector3.zero;
    private Vector3 magRatio = Vector3.zero;

    private Vector3 accMax = Vector3.zero;
    private Vector3 accMin = Vector3.zero;
    private Vector3 accRatio = Vector3.zero;

    [SerializeField]
    private GameObject _3Dmodel;


    //slideShow Variable
    private Boolean FullScreenMediaDisplayActivated = false;
    [SerializeField]
    private GameObject _ImageDisplay;
    [SerializeField]
    private GameObject VideoPlayer;
    private UnityEngine.Video.VideoPlayer videoPlayer;
    private AudioSource audioPlayer;
    UnityEngine.UI.Image image;
    private Boolean isVideoPaused = false;
    private int currentSlide = 0;
    [SerializeField]
    private Sprite[] slideOne;
    [SerializeField]
    private Sprite[] slideTwo;
    [SerializeField]
    private Sprite[] slideThree;
    [SerializeField]
    private Sprite[] slideFour;
    [SerializeField]
    private Sprite[] slideFive;
    [SerializeField]
    private Sprite[] slideSix;
    [SerializeField]
    private AudioClip slideOneAudio;
    [SerializeField]
    private AudioClip slideTwoAudio;
    [SerializeField]
    private AudioClip slideThreeAudio;
    [SerializeField]
    private AudioClip slideFourAudio;
    [SerializeField]
    private AudioClip slideFiveAudio;
    [SerializeField]
    private AudioClip slideSixAudio;


    [SerializeField]
    private UnityEngine.Video.VideoClip videoClipOne;
    [SerializeField]
    private AudioClip audioClipOne;


    //coroutine variable;
    private Coroutine previousCoroutine;
    private int currentCoroutine = -1;

    //Camera Variable;
    [SerializeField]
    private GameObject[] camera_position;
    private Vector3 defaultCameraPosition;
    private Quaternion defaultCameraRotation;

    //Boolean used by coroutine to notify the fixed update that it should take a new Offset.
    void resetOffset()
    {
        changeOffsetQuat = true;
    }
    //Coroutine that wait for the calibration to end, then wait 5 seconds before taking the current position as the one to offset.
    //After this offset, the object should be facing the user.
    //Ask Daniel for the full calibration process. He has a document that sumeraze them.
    IEnumerator getQuatOffset()
    {
        Debug.Log("Hold Standing still");
        int secondesToWait = 5;
        //Let some time for object to be in rotation to offset.
        while (secondesToWait > 0)
        {
            //wait for the mag calibration to be finished before starting counter.
            if (offsetted)
            {
                secondesToWait--;
            }
            yield return new WaitForSeconds(1);
        }
        changeOffsetQuat = true;
        Debug.Log("Finished getting offset");
    }

    //Coroutine that run a scanWifi until it reads in its return the String "ESP8266 Thing" which show that at least one Sensor has created a wifi to connect.
    //Windows should be in automatic connection to this wifi.
    // it set the boolean connected to true, which is one of the condition for the fixedUpdate to run with offseted.
    // it set the MQTT Client to connect to 192.168.4.2 which is the default IP that the computer will get once connected to the sensor.
    // it then subscribe to the topic "raw_data" which is one the sensor publish its data to.
    IEnumerator wifiCheck()
    {
        connected = false;
        while (!connected)
        {
            String scanResult = scanWifi();
            if (scanResult.Contains("ESP8266 Thing"))
            {
                connected = true;
            }
            yield return new WaitForSeconds(1);
        }
        if (connected)
        {

            MqttClient client = new MqttClient(IPAddress.Parse("192.168.4.2"));


            string clientId = Guid.NewGuid().ToString();
            client.Connect(clientId);

            client.Subscribe(new string[] { "raw_data" }, new byte[] {
                MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE
            });

            client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;

        }

    }

    //Function that run WINDOWS command in order to look for available wifi.
    //return the result of windows command as a string.
    string scanWifi()
    {
        //System.Diagnostics.Process.Start("/c netsh wlan show networks mode=bssid");
        string cmd = "/c netsh wlan show networks mode=bssid";
        System.Diagnostics.Process proc = new System.Diagnostics.Process();
        proc.StartInfo.FileName = "cmd.exe";
        proc.StartInfo.Arguments = cmd;
        proc.StartInfo.UseShellExecute = false;
        proc.StartInfo.RedirectStandardOutput = true;
        proc.StartInfo.CreateNoWindow = true;
        proc.Start();
        return proc.StandardOutput.ReadToEnd();


    }

    //init function, should be run once.
    //Make the button default state 0 for OFF
    void initButtonVariable()
    {
        for (int i = 0; i < 8; i++)
        {
            buttonPreviousState[i] = "0";
            buttonCurrentState[i] = "O";
        }
        
    }

    //This function will make transparent all children of an object except for the first and the last.
    //If the object have children, it will make them transparent instead ( object combined of multiple parts)
    //The first children is the first one to be displayed
    //The last children doesn't have any shadder and should be the button/camera_position.
    void initTransparency(GameObject go)
    {
        for (int i = 1; i < go.transform.childCount-1; i++)
        {
            Transform child = go.transform.GetChild(i);
            if (child.childCount > 0)
            {
                for (int j = 0; j < child.childCount; j++)
                {
                    Transform children = child.GetChild(j);
                    Renderer renderer = children.gameObject.GetComponent<MeshRenderer>();
                    renderer.material.SetFloat("_Mode", 2f);
                    float f = 0;
                    Color c = renderer.material.color;
                    c.a = f;
                    renderer.material.color = c;
                }
            }
            else
            {

                Renderer renderer = child.gameObject.GetComponent<MeshRenderer>();
                renderer.material.SetFloat("_Mode", 2f);
                float f = 0;
                Color c = renderer.material.color;
                c.a = f;
                renderer.material.color = c;
            }
        }

    }

    //Function that receive Mqtt message from the sensor.
    //We first transform it as a string, then spit the string at each space.
    //Then, we must parse each value into it's correct type.
    //This message are in the form:
    // rd ax ay az mx my mg gx gy gz ed roll pitch yaw ba 0 1 2 3 4 5 6 7
    // rd stands for raw_data
    // ed stands for euler_data ( might be deleted in future for performance purpose )
    // ba stands for button_actioned
    // ax, ay, az are 3 floats that store the data of the accelerometers along each axis
    // mx, my,maz are 3 floats that store the data of the magnetometer along each axis
    // gx, gy, gz are 3 floats that store the data of the gyroscope along each axis
    // roll pitch yaw, are standard euler Orientation system, but are subject to Gimbal Lock.
    // 0 1 2 3 4 5 6 7 are the buttons value either 0 or 1, which is the current digitalState of the button number 0 1 2 3 4 5 6 7.
    
    void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
    {
        ////Debug.Log("Received = " + Encoding.UTF8.GetString(e.Message) + " on topic " + e.Topic);
        string message = Encoding.UTF8.GetString(e.Message);

        String[] parsed = message.Split();

        //BUTTON DATA PARSING PART..................................................\\
        //
        for (int i = 15; i < 23; i++)
        {
            buttonCurrentState[i - 15] = parsed[i];
        }

        //Euler Data Parsing.
        Orientation.x = float.Parse(parsed[11],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        Orientation.y = float.Parse(parsed[12],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        Orientation.z = float.Parse(parsed[13],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);


        if (offsetted)
        {
            //parse the sensor data.
            imua.x = float.Parse(parsed[1],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imua.y = float.Parse(parsed[2],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imua.z = float.Parse(parsed[3],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imum.x = float.Parse(parsed[4],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imum.y = float.Parse(parsed[5],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imum.z = float.Parse(parsed[6],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imug.x = float.Parse(parsed[7],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imug.y = float.Parse(parsed[8],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imug.z = float.Parse(parsed[9],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            

            //correct for hard iron distortion;
            imum -= imuoffm;
            //correct for soft iron ( only for the distance, not the angle)
            imum.x *= magRatio.x;
            imum.y *= magRatio.y;
            imum.z *= magRatio.z;


            //Store the previous value, could be use for stabilization purpose.
            prevImuA[historyIndex] = imua;
            prevImuG[historyIndex] = imug;
            prevImuM[historyIndex] = imum;
            historyIndex = (historyIndex + 1) % maxSizeOfHistory;
            
        }
        else
        {
            //Get the first value of the Sensor in order to compute a correction for Hard and Soft Iron offset
            imuoffa.x = float.Parse(parsed[1],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffa.y = float.Parse(parsed[2],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffa.z = float.Parse(parsed[3],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffm.x = float.Parse(parsed[4],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffm.y = float.Parse(parsed[5],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffm.z = float.Parse(parsed[6],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffg.x = float.Parse(parsed[7],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffg.y = float.Parse(parsed[8],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffg.z = float.Parse(parsed[9],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            
            //mag max and min will be used for hardIron and soft Iron correction
            if (magMax.x < imuoffm.x)
            {
                magMax.x = imuoffm.x;
            }
            if (magMax.y < imuoffm.y)
            {
                magMax.y = imuoffm.y;
            }
            if (magMax.z < imuoffm.z)
            {
                magMax.z = imuoffm.z;
            }
            if (magMin.x > imuoffm.x)
            {
                magMin.x = imuoffm.x;
            }
            if (magMin.y > imuoffm.y)
            {
                magMin.y = imuoffm.y;
            }
            if (magMin.z > imuoffm.y)
            {
                magMin.z = imuoffm.y;
            }
            

            if (initCount < maxSizeOfHistory)
            {
                Debug.Log("Initialisation at " + initCount / 5 + "%");
                prevImuA[historyIndex] = imuoffa;
                prevImuG[historyIndex] = imuoffg;
                prevImuM[historyIndex] = imuoffm;

                historyIndex = (historyIndex + 1) % maxSizeOfHistory;
                initCount++;


            }
            else
            {
                //mag calibration factor computation
                //get the offset from 0 for hard iron correction
                imuoffm.x = (magMax.x + magMin.x) / 2;
                imuoffm.y = (magMax.y + magMin.y) / 2;
                imuoffm.z = (magMax.z + magMin.z) / 2;
                //get the max distance of our ellipse
                magRatio.x = Mathf.Abs(magMax.x) + Mathf.Abs(magMin.x);
                magRatio.y = Mathf.Abs(magMax.y) + Mathf.Abs(magMin.y);
                magRatio.z = Mathf.Abs(magMax.z) + Mathf.Abs(magMin.z);
                //prepare factor for correction
                float xMul = 0;
                float yMul = 0;
                float zMul = 0;
                //compute which axis should be larger
                if ((magRatio.x > magRatio.y) && (magRatio.x > magRatio.z))
                {
                    xMul = 1 / magRatio.x;
                    yMul = 1 / magRatio.y;
                    zMul = 1 / magRatio.z;
                }
                else if ((magRatio.y > magRatio.x) && (magRatio.y > magRatio.z))
                {
                    xMul = 1 / magRatio.x;
                    yMul = 1 / magRatio.y;
                    zMul = 1 / magRatio.z;
                }
                else if ((magRatio.z > magRatio.x) && (magRatio.z > magRatio.y))
                {
                    xMul = 1 / magRatio.x;
                    yMul = 1 / magRatio.y;
                    zMul = 1 / magRatio.z;
                }
                //store the factor in order to have a circle instead of an ellipse
                magRatio.x = xMul;
                magRatio.y = yMul;
                magRatio.z = zMul;
                //This Debug show the computed correction given to Magnetometer. Could be used for Hard Correction.
                //Debug.Log("magRatio:" + magRatio);
                //Debug.Log("imuoffm:" + imuoffm);
                offsetted = true;
                initCount = 0;
            }
        }

    }

    //This coroutine does in this order:
    //Make sure that all variable are default ( usefull if a button is pressed while a coroutine is already started )
    //Place the camera in a position determined by camera_position.
    //It will then get the right audio and slideShow and play them.
    //It will look regularly for the state of an int which is increased each time the same button is pressed and display the corresponding slide.
    //It stops when the slideShow is finished, no matter the state of the audio.
    //Finally it resets variables to get back to the normal camera view.
    IEnumerator buttonZoomThenPlaySlideShow(int buttonNumber)
    {
        FullScreenMediaDisplayActivated = true;
        resetImageAndVideo();
        GameObject button = camera_position[buttonNumber];
        int localDisplayedChild = -1;
        
        float lerpTimeStart = 3f;
        for (float t = 0.0f; t < lerpTimeStart; t += Time.deltaTime)
        {
            {
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, button.transform.position, t / lerpTimeStart / 2);
                Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, button.transform.rotation, t / lerpTimeStart / 2);
                yield return new WaitForSeconds(0.01f);
            }
        }
        Sprite[] currentSlideShow;
        AudioClip currentAudioClip;
        switch (buttonNumber)
        {
            case 0:
            case 1:
                currentSlideShow = slideOne;
                currentAudioClip = slideOneAudio;
                break;
            case 2:
                currentSlideShow = slideTwo;
                currentAudioClip = slideTwoAudio;
                break;
            case 3:
                currentSlideShow = slideThree;
                currentAudioClip = slideTwoAudio;
                break;
            case 4:
                currentSlideShow = slideFour;
                currentAudioClip = slideFourAudio;
                break;
            case 5:
                currentSlideShow = slideFive;
                currentAudioClip = slideFiveAudio;
                break;
            case 6:
                currentSlideShow = slideSix;
                currentAudioClip = slideSixAudio;
                break;
            default:
                currentSlideShow = null;
                currentAudioClip = null;
                break;
        }
        
        Color c = image.color;
        c.a = 1;
        image.color = c;
        audioPlayer.clip = currentAudioClip;
        audioPlayer.Play();
        while (currentSlide < currentSlideShow.Length)
        {
            if (localDisplayedChild != currentSlide)
            {
                image.sprite = slideOne[currentSlide];
                localDisplayedChild = currentSlide;
            }
            yield return new WaitForSeconds(2f);
        }
        currentCoroutine = -1;
        FullScreenMediaDisplayActivated = false;
        resetImageAndVideo();
        resetCamera();
    }


    //This coroutine does in this order:
    //Make sure that all variable are default ( usefull if a button is pressed while a coroutine is already started )
    //Place the camera in a position determined by camera_position.
    //It will then get the right audio and video clip and play them.
    //It will look regularly for the state of a boolean responsible for pausing/resuming the display until the video is finished playing.
    //Finally it resets variables to get back to the normal camera view.
    IEnumerator buttonZoomThenPlayVideo(int buttonNumber)
    {
        FullScreenMediaDisplayActivated = true;
        resetImageAndVideo();
        GameObject button = camera_position[buttonNumber];
        Debug.Log(button.transform.position + " but " + button.transform.rotation);
        float lerpTimeStart = 3f;
        bool isPausedLocal = false;
        for (float t = 0.0f; t < lerpTimeStart; t += Time.deltaTime)
        {
            {
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, button.transform.position, t / lerpTimeStart/2);
                Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, button.transform.rotation, t / lerpTimeStart/2);
                yield return new WaitForSeconds(0.01f);
            }
        }
        UnityEngine.Video.VideoClip currentVideoClip;
        AudioClip currentAudioClip;
        switch (buttonNumber)
        {
            //case 0 is for debuging with only one button available
            case 0:
            case 7:
                currentVideoClip = videoClipOne;
                currentAudioClip = audioClipOne;
                break;
            default:
                currentVideoClip = null;
                currentAudioClip = null;
                break;
        }
        videoPlayer.clip = currentVideoClip;
        audioPlayer.clip = currentAudioClip;
        videoPlayer.SetTargetAudioSource(0, audioPlayer);
        videoPlayer.Play();
        ulong nbOfFrame = videoPlayer.clip.frameCount;
        ulong currentFrame = 0;
        while (currentFrame < nbOfFrame) {
            currentFrame = (ulong)videoPlayer.frame;
            if (isVideoPaused && !isPausedLocal)
            {
                videoPlayer.Pause();
                isPausedLocal = true;
            }
            else if(!isVideoPaused && isPausedLocal)
            {
                videoPlayer.Play();
                isPausedLocal = false;
            }
            yield return new WaitForSeconds(0.1f);
        }        
        currentCoroutine = -1;
        FullScreenMediaDisplayActivated = false;
        resetImageAndVideo();
        resetCamera();
    }
    
    //Function called at the beginning and end of coroutines "ButtonZoomThen..." in order to make sure that all audio/video stop and that the SlideShowDisplay is hidden ( alpha = 0)
    void resetImageAndVideo()
    {
        audioPlayer.Stop();
        Color c = image.color;
        c.a = 0;
        image.color = c;
        videoPlayer.enabled = false;
        videoPlayer.enabled = true;
    }
        
    //Put the Camera in the position it was at StartUp.
    void resetCamera()
    {
        
        Camera.main.transform.SetPositionAndRotation(defaultCameraPosition, defaultCameraRotation);
    }


    //Coroutine that FadeOut the currentDisplayedChild or the children of the currentDisplayedChild if there is any.
    //children are used in case of composed object.
    IEnumerator FadeOut()
    {
        Transform child = _3Dmodel.transform.GetChild(currentDisplayedChild);
        if (child.childCount > 0)
        {
            Renderer[] renderers = new Renderer[child.childCount];
            for (int i = 0; i < child.childCount; i++)
            {
                renderers[i] = child.GetChild(i).GetComponent<MeshRenderer>();
                renderers[i].material.SetFloat("_Mode", 2f);

            }
            for (float f = 1f; f >= 0; f -= 0.01f)
            {
                for (int i = 0; i < child.childCount; i++)
                {
                    Color c = renderers[i].material.color;
                    c.a = f;
                    renderers[i].material.color = c;
                }
                yield return new WaitForSeconds(0.01f);
            }
        }
        else
        {
            Renderer renderer = child.gameObject.GetComponent<MeshRenderer>();
            renderer.material.SetFloat("_Mode", 2f);
            for (float f = 1f; f >= 0; f -= 0.01f)
            {
                Color c = renderer.material.color;
                c.a = f;
                renderer.material.color = c;
                yield return new WaitForSeconds(0.01f);
            }
        }
    }

    //Coroutine that FadeIn the currentDisplayedChild or the children of the currentDisplayedChild if there is any.
    //children are used in case of composed object.
    IEnumerator FadeIn()
    {
        Transform child = _3Dmodel.transform.GetChild(currentDisplayedChild);
        if (child.childCount > 0)
        {
            Renderer[] renderers = new Renderer[child.childCount];
            for (int i = 0; i < child.childCount; i++)
            {
                renderers[i] = child.GetChild(i).GetComponent<MeshRenderer>();
                renderers[i].material.SetFloat("_Mode", 2f);

            }
            for (float f = 0f; f <= 1; f += 0.01f)
            {
                for (int i = 0; i < child.childCount; i++)
                {
                    Color c = renderers[i].material.color;
                    c.a = f;
                    renderers[i].material.color = c;
                }
                yield return new WaitForSeconds(0.01f);
            }
        }
        else
        {
            Renderer renderer = child.gameObject.GetComponent<MeshRenderer>();
            renderer.material.SetFloat("_Mode", 2f);
            for (float f = 0f; f <= 1; f += 0.01f)
            {
                Color c = renderer.material.color;
                c.a = f;
                renderer.material.color = c;
                yield return new WaitForSeconds(0.01f);
            }

            renderer.material.SetFloat("_Mode", 0f);
        }

    }
    //Start the fadingOut of the currently displayed object and start fadingIn the next Object.
    //It will iterate in all children of _3Dmodel but the last one which should be the button_position
    void changeModel()
    {

        StartCoroutine("FadeOut");
        currentDisplayedChild = (currentDisplayedChild + 1) % _3Dmodel.transform.childCount-1;
        StartCoroutine("FadeIn");


    }

    //function historically used by coroutine to inform FixedUpdate to change model.
    //TODO - Do this inside of TakeActionButton();
    void nextModel()
    {
        mustChangeModel = true;
    }
    
    //Compute the average Vector of an Vector3 array.
    //It has been used to stabilize the object and avoid shaking.
    //The size of Vector3[] history should not be to large ( more than 100 ) in order to avoid delay
    Vector3 getAverage(Vector3[] history)
    {
        Vector3 result = Vector3.zero;
        int i = 0;
        for (i = 0; i < history.Length; i++)
        {

            result += history[i];


        }
        result = result / history.Length;
        return result;

    }
    void Start()
    {
        //Init of Video/Audio/Image player
        videoPlayer = VideoPlayer.GetComponent<UnityEngine.Video.VideoPlayer>();
        audioPlayer = VideoPlayer.GetComponent<AudioSource>();
        image = _ImageDisplay.GetComponent<UnityEngine.UI.Image>();

        //Get the Camera initial pos and rot in order to put them back with resetCamera();
        defaultCameraPosition = Camera.main.transform.position;
        defaultCameraRotation = Camera.main.transform.rotation;
        
        //Check for wifi, used to stop the application as long as we're not connecting and avoid crash at startup.
        //We also initialise previousCoroutine with this in order to avoid nullReference later.
        previousCoroutine = StartCoroutine("wifiCheck");

        //Init Array responsible for button value recording.
        initButtonVariable();
        //Make the First child of _3DModel visible and hide all others minus the last which should be the button_position
        initTransparency(_3Dmodel);
        //This coroutine will wait for the calibration to end and then take the correct offset to apply.
        StartCoroutine("getQuatOffset");

    }

    //Function that start/stop/change coroutine in function of Button pressed;
    void takeActionButton()
    {
        //We check that the button is active , and that it's previous state was different.
        for (int i = 0; i < 8; i++) {
            if (buttonCurrentState[i].Contains("1") && buttonPreviousState[i] != buttonCurrentState[i] &&offsetted)
            {

                
                //Switch case used to decide on an action for each button.
                switch (i)
                {
                    case 0:
                        if (currentCoroutine != i)
                        {
                            currentSlide = 0;
                            if (previousCoroutine != null)
                            {
                                StopCoroutine(previousCoroutine);
                            }
                            previousCoroutine = StartCoroutine(buttonZoomThenPlaySlideShow(i));
                            currentCoroutine = i;
                        }
                        else
                        {
                            currentSlide = (currentSlide + 1);
                        }
                        //nextModel();
                        //Debug.Log("resetOffset");
                        //resetOffset();
                        break; /* optional */
                    case 1:
                        if(currentCoroutine != i)
                        {
                            currentSlide = 0;
                            if (previousCoroutine != null)
                            {
                                StopCoroutine(previousCoroutine);
                            }
                            previousCoroutine = StartCoroutine(buttonZoomThenPlaySlideShow(i));
                            currentCoroutine = i;
                        }
                        else
                        {
                            currentSlide = (currentSlide + 1);
                        }
                        break;
                    case 2:
                        if (currentCoroutine != i)
                        {
                            currentSlide = 0;
                            if (previousCoroutine != null)
                            {
                                StopCoroutine(previousCoroutine);
                            }
                            previousCoroutine = StartCoroutine(buttonZoomThenPlaySlideShow(i));
                            currentCoroutine = i;
                        }
                        else
                        {
                            currentSlide = (currentSlide + 1);
                        }
                        break;
                    case 3:
                        if (currentCoroutine != i)
                        {
                            currentSlide = 0;
                            if (previousCoroutine != null)
                            {
                                StopCoroutine(previousCoroutine);
                            }
                            previousCoroutine = StartCoroutine(buttonZoomThenPlaySlideShow(i));
                            currentCoroutine = i;
                        }
                        else
                        {
                            currentSlide = (currentSlide + 1);
                        }
                        break;
                    case 4:
                        if (currentCoroutine != i)
                        {
                            currentSlide = 0;
                            if (previousCoroutine != null)
                            {
                                StopCoroutine(previousCoroutine);
                            }
                            previousCoroutine = StartCoroutine(buttonZoomThenPlaySlideShow(i));
                            currentCoroutine = i;
                        }
                        else
                        {
                            currentSlide = (currentSlide + 1);
                        }
                        break;
                    case 5:
                        if (currentCoroutine != i)
                        {
                            currentSlide = 0;
                            if (previousCoroutine != null)
                            {
                                StopCoroutine(previousCoroutine);
                            }
                            previousCoroutine = StartCoroutine(buttonZoomThenPlaySlideShow(i));
                            currentCoroutine = i;
                        }
                        else
                        {
                            currentSlide = (currentSlide + 1);
                        }
                        break;
                    case 6:
                        if (currentCoroutine != i)
                        {
                            currentSlide = 0;
                            if (previousCoroutine != null)
                            {
                                StopCoroutine(previousCoroutine);
                            }
                            previousCoroutine = StartCoroutine(buttonZoomThenPlaySlideShow(i));
                            currentCoroutine = i;
                        }
                        else
                        {
                            currentSlide = (currentSlide + 1);
                        }
                        break;
                    case 7:
                        if (currentCoroutine != i)
                        {
                            isVideoPaused = false;
                            if (previousCoroutine != null)
                            {
                                StopCoroutine(previousCoroutine);
                            }
                            previousCoroutine = StartCoroutine(buttonZoomThenPlayVideo(i));
                            currentCoroutine = i;
                        }
                        else
                        {
                            isVideoPaused = !isVideoPaused;
                        }
                        break;
                    /* you can have any number of case statements */
                    default: /* Optional */
                        //Debug.Log("The button" + button_number + " has no action");
                        break;
                }
                

            }
            buttonPreviousState[i] = buttonCurrentState[i];
        }
    }


    void FixedUpdate()
    {

        if (connected)
        {
            //care should be given to argument order because referential might be different ( and it is different from unity to sparkfun IMU ).
            MadgwickAHRSupdate(dpsToRps(imug.y), dpsToRps(imug.x), dpsToRps(imug.z), imua.y, imua.x, imua.z, imum.y, -imum.x, imum.z);
            if (!FullScreenMediaDisplayActivated)
            {

                Quaternion temp = Quaternion.Inverse(offQuat) * q;
                Vector3 tempTemp = temp.eulerAngles;
                tempTemp.x = -tempTemp.x;
                tempTemp.y = -tempTemp.y;
                temp = Quaternion.Euler(tempTemp);
                _3Dmodel.transform.rotation = temp;
            }
            //this function start/stop/pause coroutine dependanding of imput.
            takeActionButton();

            //Take a new offset when the calibration is finished
            if (changeOffsetQuat)
            {
                offQuat = q;
                Debug.Log(q);
                changeOffsetQuat = false;
            }
            //Change the model. Could be done directly in TakeActionButton();
            if (mustChangeModel)
            {
                changeModel();
                mustChangeModel = false;
            }
            

        }
        else
        {
            //Debug.Log("WaitForConnection");
        }

    }


    //====================================================================================================
    // Functions for Madgwick

    //---------------------------------------------------------------------------------------------------
    // AHRS algorithm update

    void MadgwickAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz)
    {
        float recipNorm;
        float s0, s1, s2, s3;
        float qDot1, qDot2, qDot3, qDot4;
        float hx, hy;
        float _2q0mx, _2q0my, _2q0mz, _2q1mx, _2bx, _2bz, _4bx, _4bz, _2q0, _2q1, _2q2, _2q3, _2q0q2, _2q2q3, q0q0, q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;

        // Use IMU algorithm if magnetometer measurement invalid (avoids NaN in magnetometer normalisation)
        if ((mx == 0.0f) && (my == 0.0f) && (mz == 0.0f))
        {
            MadgwickAHRSupdateIMU(gx, gy, gz, ax, ay, az);
            return;
        }

        // Rate of change of quaternion from gyroscope
        qDot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
        qDot2 = 0.5f * (q0 * gx + q2 * gz - q3 * gy);
        qDot3 = 0.5f * (q0 * gy - q1 * gz + q3 * gx);
        qDot4 = 0.5f * (q0 * gz + q1 * gy - q2 * gx);

        // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if (!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f)))
        {

            // Normalise accelerometer measurement
            recipNorm = invSqrt(ax * ax + ay * ay + az * az);

            ax *= recipNorm;
            ay *= recipNorm;
            az *= recipNorm;

            // Normalise magnetometer measurement
            recipNorm = invSqrt(mx * mx + my * my + mz * mz);
            mx *= recipNorm;
            my *= recipNorm;
            mz *= recipNorm;

            // Auxiliary variables to avoid repeated arithmetic
            _2q0mx = 2.0f * q0 * mx;
            _2q0my = 2.0f * q0 * my;
            _2q0mz = 2.0f * q0 * mz;
            _2q1mx = 2.0f * q1 * mx;
            _2q0 = 2.0f * q0;
            _2q1 = 2.0f * q1;
            _2q2 = 2.0f * q2;
            _2q3 = 2.0f * q3;
            _2q0q2 = 2.0f * q0 * q2;
            _2q2q3 = 2.0f * q2 * q3;
            q0q0 = q0 * q0;
            q0q1 = q0 * q1;
            q0q2 = q0 * q2;
            q0q3 = q0 * q3;
            q1q1 = q1 * q1;
            q1q2 = q1 * q2;
            q1q3 = q1 * q3;
            q2q2 = q2 * q2;
            q2q3 = q2 * q3;
            q3q3 = q3 * q3;

            // Reference direction of Earth's magnetic field
            hx = mx * q0q0 - _2q0my * q3 + _2q0mz * q2 + mx * q1q1 + _2q1 * my * q2 + _2q1 * mz * q3 - mx * q2q2 - mx * q3q3;
            hy = _2q0mx * q3 + my * q0q0 - _2q0mz * q1 + _2q1mx * q2 - my * q1q1 + my * q2q2 + _2q2 * mz * q3 - my * q3q3;
            _2bx = Mathf.Sqrt(hx * hx + hy * hy);
            _2bz = -_2q0mx * q2 + _2q0my * q1 + mz * q0q0 + _2q1mx * q3 - mz * q1q1 + _2q2 * my * q3 - mz * q2q2 + mz * q3q3;
            _4bx = 2.0f * _2bx;
            _4bz = 2.0f * _2bz;

            // Gradient decent algorithm corrective step
            s0 = -_2q2 * (2.0f * q1q3 - _2q0q2 - ax) + _2q1 * (2.0f * q0q1 + _2q2q3 - ay) - _2bz * q2 * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * q3 + _2bz * q1) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * q2 * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
            s1 = _2q3 * (2.0f * q1q3 - _2q0q2 - ax) + _2q0 * (2.0f * q0q1 + _2q2q3 - ay) - 4.0f * q1 * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) + _2bz * q3 * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * q2 + _2bz * q0) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * q3 - _4bz * q1) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
            s2 = -_2q0 * (2.0f * q1q3 - _2q0q2 - ax) + _2q3 * (2.0f * q0q1 + _2q2q3 - ay) - 4.0f * q2 * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) + (-_4bx * q2 - _2bz * q0) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * q1 + _2bz * q3) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * q0 - _4bz * q2) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
            s3 = _2q1 * (2.0f * q1q3 - _2q0q2 - ax) + _2q2 * (2.0f * q0q1 + _2q2q3 - ay) + (-_4bx * q3 + _2bz * q1) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * q0 + _2bz * q2) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * q1 * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
            recipNorm = invSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3); // normalise step magnitude
            s0 *= recipNorm;
            s1 *= recipNorm;
            s2 *= recipNorm;
            s3 *= recipNorm;

            // Apply feedback step
            qDot1 -= beta * s0;
            qDot2 -= beta * s1;
            qDot3 -= beta * s2;
            qDot4 -= beta * s3;
        }

        // Integrate rate of change of quaternion to yield quaternion
        q0 += qDot1 * (1.0f / sampleFreq);
        q1 += qDot2 * (1.0f / sampleFreq);
        q2 += qDot3 * (1.0f / sampleFreq);
        q3 += qDot4 * (1.0f / sampleFreq);

        // Normalise quaternion
        recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
        q0 *= recipNorm;
        q1 *= recipNorm;
        q2 *= recipNorm;
        q3 *= recipNorm;

        q.w = q0;
        q.x = q1;
        q.y = q2;
        q.z = q3;

    }

    //---------------------------------------------------------------------------------------------------
    // IMU algorithm update

    void MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az)
    {
        float recipNorm;
        float s0, s1, s2, s3;
        float qDot1, qDot2, qDot3, qDot4;
        float _2q0, _2q1, _2q2, _2q3, _4q0, _4q1, _4q2, _8q1, _8q2, q0q0, q1q1, q2q2, q3q3;

        // Rate of change of quaternion from gyroscope
        qDot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
        qDot2 = 0.5f * (q0 * gx + q2 * gz - q3 * gy);
        qDot3 = 0.5f * (q0 * gy - q1 * gz + q3 * gx);
        qDot4 = 0.5f * (q0 * gz + q1 * gy - q2 * gx);

        // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if (!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f)))
        {

            // Normalise accelerometer measurement
            recipNorm = invSqrt(ax * ax + ay * ay + az * az);
            ax *= recipNorm;
            ay *= recipNorm;
            az *= recipNorm;

            // Auxiliary variables to avoid repeated arithmetic
            _2q0 = 2.0f * q0;
            _2q1 = 2.0f * q1;
            _2q2 = 2.0f * q2;
            _2q3 = 2.0f * q3;
            _4q0 = 4.0f * q0;
            _4q1 = 4.0f * q1;
            _4q2 = 4.0f * q2;
            _8q1 = 8.0f * q1;
            _8q2 = 8.0f * q2;
            q0q0 = q0 * q0;
            q1q1 = q1 * q1;
            q2q2 = q2 * q2;
            q3q3 = q3 * q3;

            // Gradient decent algorithm corrective step
            s0 = _4q0 * q2q2 + _2q2 * ax + _4q0 * q1q1 - _2q1 * ay;
            s1 = _4q1 * q3q3 - _2q3 * ax + 4.0f * q0q0 * q1 - _2q0 * ay - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * az;
            s2 = 4.0f * q0q0 * q2 + _2q0 * ax + _4q2 * q3q3 - _2q3 * ay - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * az;
            s3 = 4.0f * q1q1 * q3 - _2q1 * ax + 4.0f * q2q2 * q3 - _2q2 * ay;
            recipNorm = invSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3); // normalise step magnitude
            s0 *= recipNorm;
            s1 *= recipNorm;
            s2 *= recipNorm;
            s3 *= recipNorm;

            // Apply feedback step
            qDot1 -= beta * s0;
            qDot2 -= beta * s1;
            qDot3 -= beta * s2;
            qDot4 -= beta * s3;
        }

        // Integrate rate of change of quaternion to yield quaternion
        q0 += qDot1 * (1.0f / sampleFreq);
        q1 += qDot2 * (1.0f / sampleFreq);
        q2 += qDot3 * (1.0f / sampleFreq);
        q3 += qDot4 * (1.0f / sampleFreq);

        // Normalise quaternion
        recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
        q0 *= recipNorm;
        q1 *= recipNorm;
        q2 *= recipNorm;
        q3 *= recipNorm;

        q.w = q0;
        q.x = q1;
        q.y = q2;
        q.z = q3;

    }

    //---------------------------------------------------------------------------------------------------
    // Fast inverse square-root
    // See: http://en.wikipedia.org/wiki/Fast_inverse_square_root


    //function used to compute the invSqrt.
    //There are other variation, more or less precise.
    float invSqrt(float x)
    {
        return 1 / Mathf.Sqrt(x);
    }


    //function used to give the rad.s^-1 when degree per seconds are given.
    float dpsToRps(float dps)
    {
        return Mathf.PI / 180 * dps;
    }




    //====================================================================================================
    // END OF CODE
    //====================================================================================================
}