﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Text;
using System;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;


public class mqttTestBis : MonoBehaviour
{

    private Vector3 Orientation;

    //offset Quaternion
    private Boolean changeOffsetQuat = false;
    private Quaternion offQuat = Quaternion.identity;

    //magnetic declination
    private float magDeclination = 0.49f;


    //button boolean
    private String[] buttonPreviousState = new String[8];

    private Quaternion q = Quaternion.identity;

    //Vector data of Imu Acceleration, Magnetometer , Gyroscope
    private Vector3 imua = Vector3.zero;
    private Vector3 imum = Vector3.zero;
    private Vector3 imug = Vector3.zero;
    //Offset for each vector of data.
    //Equal to the first data received
    private Vector3 imuoffa = Vector3.zero;
    private Vector3 imuoffm = Vector3.zero;
    private Vector3 imuoffg = Vector3.zero;
    //Average of the [maxSizeOfHistory] previous result.
    //used to reduce noise
    private Vector3 noiseImua = Vector3.zero;
    private Vector3 noiseImum = Vector3.zero;
    private Vector3 noiseImug = Vector3.zero;
    //variable used for stocking previous result.
    private static int maxSizeOfHistory = 500;
    private int historyIndex = 0;
    //counter for the init ( should go until maxSizeOfHistory)
    private int initCount = 0;
    //counter for noise/need to update
    private int noiseCounter;

    //Vector array used to stock previous result of each Imu Vector.
    private Vector3[] prevImuA = new Vector3[maxSizeOfHistory];
    private Vector3[] prevImuM = new Vector3[maxSizeOfHistory];
    private Vector3[] prevImuG = new Vector3[maxSizeOfHistory];

    //Bool to check is the offset has been computed.
    private bool offsetted = false;

    //variable used by Madgwick in order to compute the quaternion from the IMU data.
    private float sampleFreq = 50;

    //---------------------------------------------------------------------------------------------------
    // Variable definitions
    //arbitrary factor for Madgwick
    private float beta = 1.5f;//0.3f;                                  // 2 * proportional gain (Kp)
                              //Default Quaternion variable for Madgwick
    private float q0 = 1.0f, q1 = 0.0f, q2 = 0.0f, q3 = 0.0f;   // quaternion of sensor frame relative to auxiliary frame

    //---------------------------------------------------------------------------------------------------
    // Function declarations
    //variable used for changing the displayed model.
    private int currentDisplayedChild = 0;
    private bool mustChangeModel = false;


    //current connexion state
    private bool connected;

    //calibration variable;
    private Vector3 magMax = Vector3.zero;
    private Vector3 magMin = Vector3.zero;
    private Vector3 magRatio = Vector3.zero;

    private Vector3 accMax = Vector3.zero;
    private Vector3 accMin = Vector3.zero;
    private Vector3 accRatio = Vector3.zero;

    [SerializeField]
    private GameObject _3Dmodel;


    IEnumerator getQuatOffset()
    {
        //Debug.Log("Hold Standing still");
        int secondesToWait = 5;
        //Let some time for object to be in rotation to offset.
        while (secondesToWait > 0)
        {
            //wait for the mag calibration to be finished before starting counter.
            if (offsetted)
            {
                secondesToWait--;
            }
            yield return new WaitForSeconds(1);
        }
        changeOffsetQuat = true;
        //Debug.Log("Finished getting offset");
    }

    //Wifi function
    IEnumerator wifiCheck()
    {
        connected = false;
        while (!connected)
        {
            String scanResult = scanWifi();
            ////Debug.Log(scanResult);
            if (scanResult.Contains("ESP8266 Thing"))
            {
                connected = true;
                ////Debug.Log("connected");
            }
            else
            {
                ////Debug.Log("not connected");
            }
            yield return new WaitForSeconds(1);
        }
        if (connected)
        {

            MqttClient client = new MqttClient(IPAddress.Parse("192.168.4.2"));//"192.168.1.159"


            string clientId = Guid.NewGuid().ToString();
            client.Connect(clientId);

            client.Subscribe(new string[] { "raw_data" }, new byte[] {
                MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE
            });

            client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;

        }

    }
    string scanWifi()
    {
        //System.Diagnostics.Process.Start("/c netsh wlan show networks mode=bssid");
        string cmd = "/c netsh wlan show networks mode=bssid";
        System.Diagnostics.Process proc = new System.Diagnostics.Process();
        proc.StartInfo.FileName = "cmd.exe";
        proc.StartInfo.Arguments = cmd;
        proc.StartInfo.UseShellExecute = false;
        proc.StartInfo.RedirectStandardOutput = true;
        proc.StartInfo.CreateNoWindow = true;
        proc.Start();
        return proc.StandardOutput.ReadToEnd();


    }

    //init function, should be run once.
    void initButtonVariable()
    {
        for (int i = 0; i < 8; i++)
        {
            buttonPreviousState[i] = "0";
        }
        StartCoroutine("wifiCheck");
    }
    void initTransparency(GameObject go)
    {
        for (int i = 1; i < go.transform.childCount; i++)
        {
            Transform child = go.transform.GetChild(i);
            if (child.childCount > 0)
            {
                for (int j = 0; j < child.childCount; j++)
                {
                    Transform children = child.GetChild(j);
                    Renderer renderer = children.gameObject.GetComponent<MeshRenderer>();
                    renderer.material.SetFloat("_Mode", 2f);
                    float f = 0;
                    Color c = renderer.material.color;
                    c.a = f;
                    renderer.material.color = c;
                }
            }
            else
            {

                Renderer renderer = child.gameObject.GetComponent<MeshRenderer>();
                renderer.material.SetFloat("_Mode", 2f);
                float f = 0;
                Color c = renderer.material.color;
                c.a = f;
                renderer.material.color = c;
            }
        }

    }


    void Start()
    {
        ////Debug.Log("Start_Begin");
        initButtonVariable();
        initTransparency(_3Dmodel);
        StartCoroutine("getQuatOffset");

    }

    //mqtt function
    void client_MqttMsgSubscribed(object sender, MqttMsgSubscribedEventArgs e)
    {
        ////Debug.Log("Subscribed for id = " + e.MessageId);
    }
    void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
    {
        //////Debug.Log("Received = " + Encoding.UTF8.GetString(e.Message) + " on topic " + e.Topic);
        string message = Encoding.UTF8.GetString(e.Message);

        String[] parsed = message.Split();

        //BUTTON DATA PARSING PART..................................................\\
        //
        for (int i = 15; i < 23; i++)
        {
            //We check that the button is active , and that it's previous state was different.
            if (parsed[i].Contains("1") && buttonPreviousState[i - 15] != parsed[i])
            {

                //Switch case used to decide on an action for each button.
                switch (i - 15)
                {
                    case 0:
                        //nextModel();
                        ////Debug.Log("resetOffset");
                        resetOffset();
                        break; /* optional */

                    /* you can have any number of case statements */
                    default: /* Optional */
                        int button_number = i - 15;
                        ////Debug.Log("The button" + button_number + " has no action");
                        break;
                }
            }
            buttonPreviousState[i - 15] = parsed[i];
        }
        Orientation.x = float.Parse(parsed[11],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat); ;
        Orientation.y = float.Parse(parsed[12],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat); ;
        Orientation.z = float.Parse(parsed[13],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat); ;


        if (offsetted)
        {

            imua.x = float.Parse(parsed[1],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imua.y = float.Parse(parsed[2],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imua.z = float.Parse(parsed[3],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imum.x = float.Parse(parsed[4],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imum.y = float.Parse(parsed[5],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imum.z = float.Parse(parsed[6],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imug.x = float.Parse(parsed[7],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imug.y = float.Parse(parsed[8],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imug.z = float.Parse(parsed[9],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);



            /*
            imua -= imuoffa;
            //imug -= imuoffg;
            imua.x *= accRatio.x;
            imua.y *= accRatio.y;
            imua.z *= accRatio.z;
            */

            //correct for hard iron distortion;
            imum -= imuoffm;
            //correct for soft iron ( only for the distance, not the angle)
            imum.x *= magRatio.x;
            imum.y *= magRatio.y;
            imum.z *= magRatio.z;



            prevImuA[historyIndex] = imua;
            prevImuG[historyIndex] = imug;
            prevImuM[historyIndex] = imum;
            historyIndex = (historyIndex + 1) % maxSizeOfHistory;


            //imua = getAverage(prevImuA);
            //imum = getAverage(prevImuM);
            //imug = getAverage(prevImuG);


            
            noiseCounter = 3;
            if (-0.05f + noiseImua.x < imua.x && imua.x < 0.05f + noiseImua.x)
            {
                imua.x = noiseImua.x;
                noiseCounter--;
            }
            else
            {
                //////Debug.Log (noiseImua.x + " c" + imua.x);
            }
            if (-0.05f + noiseImua.y < imua.y && imua.y < 0.05f + noiseImua.y)
            {
                imua.y = noiseImua.y;
                noiseCounter--;
            }
            if (-0.05f + noiseImua.z < imua.z && imua.z < 0.05f + noiseImua.z)
            {
                imua.z = noiseImua.z;
                noiseCounter--;
            }
            if (noiseCounter > 0)
            {
                //////Debug.Log(q + " " + imug + " " + imua + " " + imum);
                noiseImua = getAverage(prevImuA);
                MadgwickAHRSupdate(dpsToRps(imug.y), dpsToRps(imug.x), dpsToRps(imug.z), imua.y, imua.x, imua.z, imum.y, -imum.x, imum.z);
            }




        }
        else
        {
            imuoffa.x = float.Parse(parsed[1],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffa.y = float.Parse(parsed[2],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffa.z = float.Parse(parsed[3],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffm.x = float.Parse(parsed[4],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffm.y = float.Parse(parsed[5],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffm.z = float.Parse(parsed[6],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffg.x = float.Parse(parsed[7],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffg.y = float.Parse(parsed[8],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            imuoffg.z = float.Parse(parsed[9],
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            //MadgwickAHRSupdate(dpsToRps(imuoffg.y), dpsToRps(imuoffg.x), dpsToRps(imuoffg.z), imuoffa.y, imuoffa.x, imuoffa.z, imuoffm.y, -imuoffm.x, imuoffm.z);

            //mag max and min 
            if (magMax.x < imuoffm.x)
            {
                magMax.x = imuoffm.x;
            }
            if (magMax.y < imuoffm.y)
            {
                magMax.y = imuoffm.y;
            }
            if (magMax.z < imuoffm.z)
            {
                magMax.z = imuoffm.z;
            }
            if (magMin.x > imuoffm.x)
            {
                magMin.x = imuoffm.x;
            }
            if (magMin.y > imuoffm.y)
            {
                magMin.y = imuoffm.y;
            }
            if (magMin.z > imuoffm.y)
            {
                magMin.z = imuoffm.y;
            }

            /*
            //acc max and min
            if (accMax.x < imuoffa.x)
            {
                accMax.x = imuoffa.x;
            }
            if (accMax.y < imuoffa.y)
            {
                accMax.y = imuoffa.y;
            }
            if (accMax.z < imuoffa.z)
            {
                accMax.z = imuoffa.z;
            }
            if (accMin.x > imuoffa.x)
            {
                accMin.x = imuoffa.x;
            }
            if (accMin.y > imuoffa.y)
            {
                accMin.y = imuoffa.y;
            }
            if (accMin.z > imuoffa.y)
            {
                accMin.z = imuoffa.y;
            }

            */


            if (initCount < maxSizeOfHistory)
            {
                //Debug.Log("Initialisation at " + initCount / 5 + "%");
                prevImuA[historyIndex] = imuoffa;
                prevImuG[historyIndex] = imuoffg;
                prevImuM[historyIndex] = imuoffm;

                historyIndex = (historyIndex + 1) % maxSizeOfHistory;
                initCount++;


            }
            else
            {
                //mag calibration factor computation
                //get the offset from 0 for hard iron correction
                imuoffm.x = (magMax.x + magMin.x) / 2;
                imuoffm.y = (magMax.y + magMin.y) / 2;
                imuoffm.z = (magMax.z + magMin.z) / 2;
                //get the max distance of our ellipse
                magRatio.x = Mathf.Abs(magMax.x) + Mathf.Abs(magMin.x);
                magRatio.y = Mathf.Abs(magMax.y) + Mathf.Abs(magMin.y);
                magRatio.z = Mathf.Abs(magMax.z) + Mathf.Abs(magMin.z);
                //prepare factor for correction
                float xMul = 0;
                float yMul = 0;
                float zMul = 0;
                //compute which axis should be larger
                if ((magRatio.x > magRatio.y) && (magRatio.x > magRatio.z))
                {
                    xMul = 1 / magRatio.x;
                    yMul = 1 / magRatio.y;
                    zMul = 1 / magRatio.z;
                }
                else if ((magRatio.y > magRatio.x) && (magRatio.y > magRatio.z))
                {
                    xMul = 1 / magRatio.x;
                    yMul = 1 / magRatio.y;
                    zMul = 1 / magRatio.z;
                }
                else if ((magRatio.z > magRatio.x) && (magRatio.z > magRatio.y))
                {
                    xMul = 1 / magRatio.x;
                    yMul = 1 / magRatio.y;
                    zMul = 1 / magRatio.z;
                }
                //store the factor in order to have a circle instead of an ellipse
                magRatio.x = xMul;
                magRatio.y = yMul;
                magRatio.z = zMul;

                /*
                //acc factor computation
                //get the offset from 0 for hard iron correction
                imuoffa.x = (accMax.x + accMin.x) / 2;
                imuoffa.y = (accMax.y + accMin.y) / 2;
                imuoffa.z = (accMax.z + accMin.z) / 2;
                //get the max distance of our ellipse
                accRatio.x = Mathf.Abs(accMax.x) + Mathf.Abs(accMin.x);
                accRatio.y = Mathf.Abs(accMax.y) + Mathf.Abs(accMin.y);
                accRatio.z = Mathf.Abs(accMax.z) + Mathf.Abs(accMin.z);
                //compute which axis should be larger
                if ((accRatio.x > accRatio.y) && (accRatio.x > accRatio.z))
                {
                    xMul = 1 / accRatio.x;
                    yMul = 1 / accRatio.y;
                    zMul = 1 / accRatio.z;
                }
                else if ((accRatio.y > accRatio.x) && (accRatio.y > accRatio.z))
                {
                    xMul = 1 / accRatio.x;
                    yMul = 1 / accRatio.y;
                    zMul = 1 / accRatio.z;
                }
                else if ((accRatio.z > accRatio.x) && (accRatio.z > accRatio.y))
                {
                    xMul = 1 / accRatio.x;
                    yMul = 1 / accRatio.y;
                    zMul = 1 / accRatio.z;
                }
                //store the factor in order to have a circle instead of an ellipse
                accRatio.x = xMul;
                accRatio.y = yMul;
                accRatio.z = zMul;
                */

                offsetted = true;

                initCount = 0;
            }
        }
        /*
        if (my == 0)
            yaw = (mx < 0) ? PI : 0;
        else
            yaw = atan2(mx, my);

        yaw -= DECLINATION * PI / 180;

        if (yaw > PI) yaw -= (2 * PI);
        else if (yaw < -PI) yaw += (2 * PI);
        else if (yaw < 0) yaw += 2 * PI;
        */



    }

    void resetOffset()
    {
        //Must do this to understand my own problem.el
        changeOffsetQuat = true;
    }


    //Fading function
    IEnumerator FadeOut()
    {
        Transform child = _3Dmodel.transform.GetChild(currentDisplayedChild);
        if (child.childCount > 0)
        {
            Renderer[] renderers = new Renderer[child.childCount];
            for (int i = 0; i < child.childCount; i++)
            {
                renderers[i] = child.GetChild(i).GetComponent<MeshRenderer>();
                renderers[i].material.SetFloat("_Mode", 2f);

            }
            for (float f = 1f; f >= 0; f -= 0.01f)
            {
                for (int i = 0; i < child.childCount; i++)
                {
                    Color c = renderers[i].material.color;
                    c.a = f;
                    renderers[i].material.color = c;
                }
                yield return new WaitForSeconds(0.01f);
            }
        }
        else
        {
            Renderer renderer = child.gameObject.GetComponent<MeshRenderer>();
            renderer.material.SetFloat("_Mode", 2f);
            for (float f = 1f; f >= 0; f -= 0.01f)
            {
                Color c = renderer.material.color;
                c.a = f;
                renderer.material.color = c;
                yield return new WaitForSeconds(0.01f);
            }
        }
    }
    IEnumerator FadeIn()
    {
        Transform child = _3Dmodel.transform.GetChild(currentDisplayedChild);
        if (child.childCount > 0)
        {
            Renderer[] renderers = new Renderer[child.childCount];
            for (int i = 0; i < child.childCount; i++)
            {
                renderers[i] = child.GetChild(i).GetComponent<MeshRenderer>();
                renderers[i].material.SetFloat("_Mode", 2f);

            }
            for (float f = 0f; f <= 1; f += 0.01f)
            {
                for (int i = 0; i < child.childCount; i++)
                {
                    Color c = renderers[i].material.color;
                    c.a = f;
                    renderers[i].material.color = c;
                }
                yield return new WaitForSeconds(0.01f);
            }
        }
        else
        {
            Renderer renderer = child.gameObject.GetComponent<MeshRenderer>();
            renderer.material.SetFloat("_Mode", 2f);
            for (float f = 0f; f <= 1; f += 0.01f)
            {
                Color c = renderer.material.color;
                c.a = f;
                renderer.material.color = c;
                yield return new WaitForSeconds(0.01f);
            }

            renderer.material.SetFloat("_Mode", 0f);
        }

    }
    //model changement
    void changeModel()
    {

        StartCoroutine("FadeOut");
        currentDisplayedChild = (currentDisplayedChild + 1) % _3Dmodel.transform.childCount;
        StartCoroutine("FadeIn");


    }
    void nextModel()
    {

        mustChangeModel = true;
    }
    /**/



    Vector3 getAverage(Vector3[] history)
    {
        Vector3 result = Vector3.zero;
        int i = 0;
        for (i = history.Length-25; i < history.Length; i++)
        {

            result += history[i];


        }
        result = result / 25;
        return result;

    }

    void FixedUpdate()
    {
        //////Debug.Log(Time.time);

        if (connected)
        {
            //care should be given to argument order because referential might be different ( and it is different from unity to sparkfun IMU ).
            
            Quaternion temp = Quaternion.Inverse(offQuat) * q;
            Vector3 tempTemp = temp.eulerAngles;
            tempTemp.x = -tempTemp.x;
            tempTemp.y = -tempTemp.y;
            temp = Quaternion.Euler(tempTemp);
            _3Dmodel.transform.rotation = temp;

            if (changeOffsetQuat)
            {
                offQuat = q;
                //Debug.Log(q);
                changeOffsetQuat = false;
            }

            if (mustChangeModel)
            {

                changeModel();
                mustChangeModel = false;

            }


        }
        else
        {
            ////Debug.Log("WaitForConnection");
        }

    }







    //====================================================================================================
    // Functions for Madgwick

    //---------------------------------------------------------------------------------------------------
    // AHRS algorithm update

    void MadgwickAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz)
    {
        float recipNorm;
        float s0, s1, s2, s3;
        float qDot1, qDot2, qDot3, qDot4;
        float hx, hy;
        float _2q0mx, _2q0my, _2q0mz, _2q1mx, _2bx, _2bz, _4bx, _4bz, _2q0, _2q1, _2q2, _2q3, _2q0q2, _2q2q3, q0q0, q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;

        // Use IMU algorithm if magnetometer measurement invalid (avoids NaN in magnetometer normalisation)
        if ((mx == 0.0f) && (my == 0.0f) && (mz == 0.0f))
        {
            MadgwickAHRSupdateIMU(gx, gy, gz, ax, ay, az);
            return;
        }

        // Rate of change of quaternion from gyroscope
        qDot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
        qDot2 = 0.5f * (q0 * gx + q2 * gz - q3 * gy);
        qDot3 = 0.5f * (q0 * gy - q1 * gz + q3 * gx);
        qDot4 = 0.5f * (q0 * gz + q1 * gy - q2 * gx);

        // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if (!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f)))
        {

            // Normalise accelerometer measurement
            recipNorm = invSqrt(ax * ax + ay * ay + az * az);

            ax *= recipNorm;
            ay *= recipNorm;
            az *= recipNorm;

            // Normalise magnetometer measurement
            recipNorm = invSqrt(mx * mx + my * my + mz * mz);
            mx *= recipNorm;
            my *= recipNorm;
            mz *= recipNorm;

            // Auxiliary variables to avoid repeated arithmetic
            _2q0mx = 2.0f * q0 * mx;
            _2q0my = 2.0f * q0 * my;
            _2q0mz = 2.0f * q0 * mz;
            _2q1mx = 2.0f * q1 * mx;
            _2q0 = 2.0f * q0;
            _2q1 = 2.0f * q1;
            _2q2 = 2.0f * q2;
            _2q3 = 2.0f * q3;
            _2q0q2 = 2.0f * q0 * q2;
            _2q2q3 = 2.0f * q2 * q3;
            q0q0 = q0 * q0;
            q0q1 = q0 * q1;
            q0q2 = q0 * q2;
            q0q3 = q0 * q3;
            q1q1 = q1 * q1;
            q1q2 = q1 * q2;
            q1q3 = q1 * q3;
            q2q2 = q2 * q2;
            q2q3 = q2 * q3;
            q3q3 = q3 * q3;

            // Reference direction of Earth's magnetic field
            hx = mx * q0q0 - _2q0my * q3 + _2q0mz * q2 + mx * q1q1 + _2q1 * my * q2 + _2q1 * mz * q3 - mx * q2q2 - mx * q3q3;
            hy = _2q0mx * q3 + my * q0q0 - _2q0mz * q1 + _2q1mx * q2 - my * q1q1 + my * q2q2 + _2q2 * mz * q3 - my * q3q3;
            _2bx = Mathf.Sqrt(hx * hx + hy * hy);
            _2bz = -_2q0mx * q2 + _2q0my * q1 + mz * q0q0 + _2q1mx * q3 - mz * q1q1 + _2q2 * my * q3 - mz * q2q2 + mz * q3q3;
            _4bx = 2.0f * _2bx;
            _4bz = 2.0f * _2bz;

            // Gradient decent algorithm corrective step
            s0 = -_2q2 * (2.0f * q1q3 - _2q0q2 - ax) + _2q1 * (2.0f * q0q1 + _2q2q3 - ay) - _2bz * q2 * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * q3 + _2bz * q1) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * q2 * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
            s1 = _2q3 * (2.0f * q1q3 - _2q0q2 - ax) + _2q0 * (2.0f * q0q1 + _2q2q3 - ay) - 4.0f * q1 * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) + _2bz * q3 * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * q2 + _2bz * q0) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * q3 - _4bz * q1) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
            s2 = -_2q0 * (2.0f * q1q3 - _2q0q2 - ax) + _2q3 * (2.0f * q0q1 + _2q2q3 - ay) - 4.0f * q2 * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) + (-_4bx * q2 - _2bz * q0) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * q1 + _2bz * q3) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * q0 - _4bz * q2) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
            s3 = _2q1 * (2.0f * q1q3 - _2q0q2 - ax) + _2q2 * (2.0f * q0q1 + _2q2q3 - ay) + (-_4bx * q3 + _2bz * q1) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * q0 + _2bz * q2) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * q1 * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
            recipNorm = invSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3); // normalise step magnitude
            s0 *= recipNorm;
            s1 *= recipNorm;
            s2 *= recipNorm;
            s3 *= recipNorm;

            // Apply feedback step
            qDot1 -= beta * s0;
            qDot2 -= beta * s1;
            qDot3 -= beta * s2;
            qDot4 -= beta * s3;
        }

        // Integrate rate of change of quaternion to yield quaternion
        q0 += qDot1 * (1.0f / sampleFreq);
        q1 += qDot2 * (1.0f / sampleFreq);
        q2 += qDot3 * (1.0f / sampleFreq);
        q3 += qDot4 * (1.0f / sampleFreq);

        // Normalise quaternion
        recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
        q0 *= recipNorm;
        q1 *= recipNorm;
        q2 *= recipNorm;
        q3 *= recipNorm;

        q.w = q0;
        q.x = q1;
        q.y = q2;
        q.z = q3;

    }

    //---------------------------------------------------------------------------------------------------
    // IMU algorithm update

    void MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az)
    {
        float recipNorm;
        float s0, s1, s2, s3;
        float qDot1, qDot2, qDot3, qDot4;
        float _2q0, _2q1, _2q2, _2q3, _4q0, _4q1, _4q2, _8q1, _8q2, q0q0, q1q1, q2q2, q3q3;

        // Rate of change of quaternion from gyroscope
        qDot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
        qDot2 = 0.5f * (q0 * gx + q2 * gz - q3 * gy);
        qDot3 = 0.5f * (q0 * gy - q1 * gz + q3 * gx);
        qDot4 = 0.5f * (q0 * gz + q1 * gy - q2 * gx);

        // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if (!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f)))
        {

            // Normalise accelerometer measurement
            recipNorm = invSqrt(ax * ax + ay * ay + az * az);
            ax *= recipNorm;
            ay *= recipNorm;
            az *= recipNorm;

            // Auxiliary variables to avoid repeated arithmetic
            _2q0 = 2.0f * q0;
            _2q1 = 2.0f * q1;
            _2q2 = 2.0f * q2;
            _2q3 = 2.0f * q3;
            _4q0 = 4.0f * q0;
            _4q1 = 4.0f * q1;
            _4q2 = 4.0f * q2;
            _8q1 = 8.0f * q1;
            _8q2 = 8.0f * q2;
            q0q0 = q0 * q0;
            q1q1 = q1 * q1;
            q2q2 = q2 * q2;
            q3q3 = q3 * q3;

            // Gradient decent algorithm corrective step
            s0 = _4q0 * q2q2 + _2q2 * ax + _4q0 * q1q1 - _2q1 * ay;
            s1 = _4q1 * q3q3 - _2q3 * ax + 4.0f * q0q0 * q1 - _2q0 * ay - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * az;
            s2 = 4.0f * q0q0 * q2 + _2q0 * ax + _4q2 * q3q3 - _2q3 * ay - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * az;
            s3 = 4.0f * q1q1 * q3 - _2q1 * ax + 4.0f * q2q2 * q3 - _2q2 * ay;
            recipNorm = invSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3); // normalise step magnitude
            s0 *= recipNorm;
            s1 *= recipNorm;
            s2 *= recipNorm;
            s3 *= recipNorm;

            // Apply feedback step
            qDot1 -= beta * s0;
            qDot2 -= beta * s1;
            qDot3 -= beta * s2;
            qDot4 -= beta * s3;
        }

        // Integrate rate of change of quaternion to yield quaternion
        q0 += qDot1 * (1.0f / sampleFreq);
        q1 += qDot2 * (1.0f / sampleFreq);
        q2 += qDot3 * (1.0f / sampleFreq);
        q3 += qDot4 * (1.0f / sampleFreq);

        // Normalise quaternion
        recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
        q0 *= recipNorm;
        q1 *= recipNorm;
        q2 *= recipNorm;
        q3 *= recipNorm;

        q.w = q0;
        q.x = q1;
        q.y = q2;
        q.z = q3;

    }

    //---------------------------------------------------------------------------------------------------
    // Fast inverse square-root
    // See: http://en.wikipedia.org/wiki/Fast_inverse_square_root


    //should be fast but has too much of an error
    /*
    float inverseSqrt(float x)
    {
        float xhalf = 0.5f * x;
        int i = BitConverter.ToInt32(BitConverter.GetBytes(x), 0);
        i = 0x5f3759df - (i >> 1);
        x = BitConverter.ToSingle(BitConverter.GetBytes(i), 0);
        x = x * (1.5f - xhalf * x * x);
        return x;
    }
    
    //not working right now, supposedly better than inverseSqrt
    float InvSqrt(float x)
    {
        
        int i = 0x5F1F1412 - ((int)x >> 1);
        float tmp = (float)i;
        return tmp * (1.69000231f - 0.714158168f * x * tmp * tmp);
    }
    */

    float invSqrt(float x)
    {
        return 1 / Mathf.Sqrt(x);
    }



    float dpsToRps(float dps)
    {
        return Mathf.PI / 180 * dps;
    }




    //====================================================================================================
    // END OF CODE
    //====================================================================================================
}
