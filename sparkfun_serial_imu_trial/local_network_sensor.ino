#include <Wire.h>
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <SparkFunLSM9DS1.h>

const int LED_PIN = 5; // Thing's onboard, green LED
const int ANALOG_PIN = A0; // The only analog pin on the Thing
const int DIGITAL_PIN = 12; // Digital pin to be read

const char WiFiAPPSK[] = "sparkfun";

//imu 
#define LSM9DS1_M  0x1E // Would be 0x1C if SDO_M is LOW
#define LSM9DS1_AG  0x6B // Would be 0x6A if SDO_AG is LOW
LSM9DS1 imu;

#define DECLINATION 0.68 // Declination (degrees) in Boulder, CO.

//mqtt data
#define mqtt_server "192.168.4.2"//"192.168.1.145"//"192.168.1.159"
#define roll_topic "roll"
#define pitch_topic "pitch"
#define yaw_topic "yaw"


WiFiServer server(80);
WiFiClient espClient;
PubSubClient client(espClient);

//clock fq variable
long lastMsg = 0;
float temp = 0.0;

void setup() {
  // put your setup code here, to run once:


  initHardware();
  setupWiFi();
  server.begin();
  client.setServer(mqtt_server, 1883);

  
  

}

void reconnect() {
  // Loop until we're reconnected
  byte ledStatus = LOW;
  while (!client.connected()) {
    // Attempt to connect
    // If you do not want to use a username and password, change next line to

    digitalWrite(LED_PIN,ledStatus);
    ledStatus = (ledStatus == HIGH)? LOW:HIGH;
    delay(100);
    if (client.connect("ESP8266Client")) {
    //if (client.connect("ESP8266Client", mqtt_user, mqtt_password)) {
      //client.subscribe("homeassistant/switch1");
      //will be needed for deep sleep ?
    } else {
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {
  /*
  WiFiClient servClient = server.available();
  if (!servClient) {
    return;
  }
  */
  if (!client.connected()) {
    Serial.println("reco");
    reconnect();
  }
  client.loop();
  digitalWrite(LED_PIN, HIGH);
  delay(500);
  digitalWrite(LED_PIN, LOW);
  delay(500);

  long now = millis();
  digitalWrite(LED_PIN,LOW);
  if (now - lastMsg > 10) {
    lastMsg = now;
    digitalWrite(LED_PIN,HIGH);

    
    

    //sensor part:
    // Update the sensor values whenever new data is available
    if ( imu.gyroAvailable() )
    {
      // To read from the gyroscope,  first call the
      // readGyro() function. When it exits, it'll update the
      // gx, gy, and gz variables with the most current data.
      imu.readGyro();
    }
    if ( imu.accelAvailable() )
    {
      // To read from the accelerometer, first call the
      // readAccel() function. When it exits, it'll update the
      // ax, ay, and az variables with the most current data.
      imu.readAccel();
    }
    if ( imu.magAvailable() )
    {
      // To read from the magnetometer, first call the
      // readMag() function. When it exits, it'll update the
      // mx, my, and mz variables with the most current data.
      imu.readMag();
    }
    
    printAttitude(imu.ax, imu.ay, imu.az, 
                 -imu.my, -imu.mx, imu.mz);

    
    //printGyro();
    //printAccel();
    //printMag();

  }

}


void setupWiFi()
{

  
  WiFi.softAPdisconnect();
  WiFi.mode(WIFI_AP);
  int channel = 9;

  // Do a little work to get a unique-ish name. Append the
  // last two bytes of the MAC (HEX'd) to "Thing-":
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  WiFi.softAPmacAddress(mac);
  String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
                 String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
  macID.toUpperCase();
  String AP_NameString = "ESP8266 Thing " + macID + " channel "+channel;

  char AP_NameChar[AP_NameString.length() + 1];
  memset(AP_NameChar, 0, AP_NameString.length() + 1);

  for (int i=0; i<AP_NameString.length(); i++)
    AP_NameChar[i] = AP_NameString.charAt(i);

  WiFi.softAP(AP_NameChar, WiFiAPPSK, channel, false);
}

void initImu(){
  
  imu.settings.device.commInterface = IMU_MODE_I2C;
  imu.settings.device.mAddress = LSM9DS1_M;
  imu.settings.device.agAddress = LSM9DS1_AG;
  
  digitalWrite(LED_PIN, HIGH);
  delay(3000);

  if (!imu.begin())
  {
    digitalWrite(LED_PIN, LOW);
    while (1)
      ;
  }
}

void initHardware()
{
  Serial.begin(9600);
  pinMode(DIGITAL_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  initImu();
}




void printAttitude(float ax, float ay, float az, float mx, float my, float mz)
{
  float roll = atan2(ay, az);
  float pitch = atan2(-ax, sqrt(ay * ay + az * az));
  
  float heading;
  if (my == 0)
    heading = (mx < 0) ? PI : 0;
  else
    heading = atan2(mx, my);
    
  heading -= DECLINATION * PI / 180;
  
  if (heading > PI) heading -= (2 * PI);
  else if (heading < -PI) heading += (2 * PI);
  else if (heading < 0) heading += 2 * PI;
  
  // Convert everything from radians to degrees:
  heading *= 180.0 / PI;
  pitch *= 180.0 / PI;
  roll  *= 180.0 / PI;

  client.publish(roll_topic, String(roll).c_str(), false);
  client.publish(pitch_topic, String(pitch).c_str(), false);
  client.publish(yaw_topic, String(heading).c_str(), false);
  
}
