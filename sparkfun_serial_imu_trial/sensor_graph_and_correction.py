#!/usr/bin/python
# -*- coding: utf-8 -*-
import paho.mqtt.client as mqtt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import numpy as np

message_received = 0
max_message = 10000
list_result = []
list_result_with_offsetax = []
list_result_with_offsetay = []
list_result_with_offsetaz = []
list_result_with_offsetmx = []
list_result_with_offsetmy = []
list_result_with_offsetmz = []
list_result_with_offsetgx = []
list_result_with_offsetgy = []
list_result_with_offsetgz = []
list_result_euler_roll = []
list_result_euler_pitch=[]
list_result_euler_yaw = []
x = []
boolean = False
offsetax = 0
offsetay = 0
offsetaz = 0
offsetmx = 0
offsetmy = 0
offsetmz = 0
offsetgx = 0
offsetgy = 0
offsetgz = 0

Xmmaxx=0
Xmminx=0
Xmmaxy=0
Xmminy=0
Xmmaxz=0
Xmminz=0

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("raw_data")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    #print(msg.topic+" "+str(msg.payload))

    global boolean
    global offsetax
    global offsetay
    global offsetaz

    global offsetmx
    global offsetmy
    global offsetmz

    global offsetgx
    global offsetgy
    global offsetgz

    global list_result_euler_roll
    global list_result_euler_pitch
    global list_result_euler_yaw

    global list_result_with_offsetax
    global list_result_with_offsetay
    global list_result_with_offsetaz
    global list_result_with_offsetmx
    global list_result_with_offsetmy
    global list_result_with_offsetmz
    global list_result_with_offsetgx
    global list_result_with_offsetgy
    global list_result_with_offsetgz

    global message_received
    global max_message

    global Xmmaxx
    global Xmminx
    global Xmmaxy
    global Xmminy
    global Xmmaxz
    global Xmminz



    temp = str(msg.payload).split()
    #temp[9] = temp[9][:-1]
    if boolean == False:
        boolean = True
    	#print("yolo"+str(temp[9]))
        offsetax = 0#float(temp[1])
        offsetay = 0#float(temp[2])
        offsetaz = 0#float(temp[3])
        offsetmx = 0#0.175#float(temp[4])
        offsetmy = 0#+0.055#float(temp[5])
        offsetmz = 0#-0.02#float(temp[6])
        offsetgx = 0#float(temp[7])
        offsetgy = 0#float(temp[8])
        offsetgz = 0


        Xmmaxx=float(temp[4])
        Xmminx=float(temp[4])
        Xmmaxy=float(temp[5])
        Xmminy=float(temp[5])
        Xmmaxz=float(temp[6])
        Xmminz=float(temp[6])
    
    list_result.append(float(temp[1]))
    list_result_with_offsetax.append(float(temp[1])-offsetax)
    list_result_with_offsetay.append(float(temp[2])-offsetay)
    list_result_with_offsetaz.append(float(temp[3])-offsetaz)

    list_result_with_offsetmx.append(float(temp[4])-offsetmx)
    list_result_with_offsetmy.append(float(temp[5])-offsetmy)
    list_result_with_offsetmz.append(float(temp[6])-offsetmz)

    list_result_with_offsetgx.append(float(temp[7])-offsetgx)
    list_result_with_offsetgy.append(float(temp[8])-offsetgy)
    list_result_with_offsetgz.append(float(temp[9])-offsetgz)

    list_result_euler_roll.append(float(temp[11]))
    list_result_euler_pitch.append(float(temp[12]))
    list_result_euler_yaw.append(float(temp[13]))

    if Xmmaxx < float(temp[4]):
        Xmmaxx = float(temp[4])
    if Xmminx > float(temp[4]):
        Xmminx = float(temp[4])
    if Xmmaxy < float(temp[5]):
        Xmmaxy = float(temp[5])
    if Xmminy > float(temp[5]):
        Xmminy = float(temp[5])
    if Xmmaxz < float(temp[6]):
        Xmmaxz = float(temp[6])
    if Xmminz > float(temp[6]):
        Xmminz = float(temp[6])

    

    
    x.append(message_received)
    if message_received%500 == 0:
        print("isAbout"+str(message_received))
    message_received +=1
    if(message_received>max_message):
        offsetmx = (Xmmaxx + Xmminx)/2
        offsetmy = (Xmmaxy + Xmminy)/2
        offsetmz = (Xmmaxz + Xmminz)/2
        ratioX = abs(Xmmaxx)+abs(Xmminx)
        ratioY = abs(Xmmaxy)+abs(Xmminy)
        ratioZ = abs(Xmmaxz)+abs(Xmminz)
        Xmul = 0
        Ymul = 0
        Zmul = 0
        if((ratioX > ratioY) & (ratioX > ratioZ)):
            Xmul=1
            Ymul=ratioX/ratioY
            Zmul=ratioX/ratioZ
        if((ratioY > ratioX) & (ratioY > ratioZ)):
            Xmul=ratioY/ratioX
            Ymul=1
            Zmul=ratioY/ratioZ
        if((ratioZ >ratioX )& (ratioZ > ratioY)):
            Xmul=ratioZ/ratioX
            Ymul=ratioY/ratioY
            Zmul=1


        for i in range(9999):
            list_result_with_offsetmx[i] = (list_result_with_offsetmx[i]-offsetmx)*ratioX
            list_result_with_offsetmy[i] = (list_result_with_offsetmy[i]-offsetmy)*ratioY
            list_result_with_offsetmz[i] = (list_result_with_offsetmz[i]-offsetmz)*ratioZ
            

        f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=True)
        ax1.plot(list_result_with_offsetax,'b.')
        ax2.plot(list_result_with_offsetay,'r.')
        ax3.plot(list_result_with_offsetaz,'g.')
        f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=True)
        ax1.plot(list_result_with_offsetmx,marker='.', color='b')
        ax2.plot(list_result_with_offsetmy,marker='.', color='r')
        ax3.plot(list_result_with_offsetmz,marker='.', color='g')
        f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=True)
        ax1.plot(list_result_with_offsetgx,marker='.', color='b')
        ax2.plot(list_result_with_offsetgy,marker='.', color='r')
        ax3.plot(list_result_with_offsetgz,marker='.', color='g')
        f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=True)
        ax1.plot(list_result_euler_roll,marker='.', color='b')
        ax2.plot(list_result_euler_pitch,marker='.', color='r')
        ax3.plot(list_result_euler_yaw,marker='.', color='g')
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(list_result_with_offsetmx, list_result_with_offsetmy, list_result_with_offsetmz, c='b', marker='.')
        ax.set_xlabel('X mag')
        ax.set_ylabel('Y mag')
        ax.set_zlabel('Z mag')
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(list_result_with_offsetmx, list_result_with_offsetmz, 'r+', zdir='z', zs=0)
        ax.set_xlabel('X mag')
        ax.set_ylabel('Y mag')
        ax.set_zlabel('Z mag')
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(list_result_with_offsetmy, list_result_with_offsetmz, 'g+', zdir='z', zs=0)
        ax.set_xlabel('X mag')
        ax.set_ylabel('Y mag')
        ax.set_zlabel('Z mag')
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(list_result_with_offsetmx, list_result_with_offsetmy, 'k+', zdir='z', zs=0)
        ax.set_xlabel('X mag')
        ax.set_ylabel('Y mag')
        ax.set_zlabel('Z mag')
        
        fig = plt.figure()
        bx = fig.add_subplot(111, projection='3d')
        bx.scatter(list_result_with_offsetax, list_result_with_offsetay, list_result_with_offsetaz, c='b', marker='.')
        bx.set_xlabel('X acc')
        bx.set_ylabel('Y acc')
        bx.set_zlabel('Z acc')
        fig = plt.figure()
        cx = fig.add_subplot(111, projection='3d')
        cx.scatter(list_result_with_offsetgx, list_result_with_offsetgy, list_result_with_offsetgz, c='b', marker='.')
        cx.set_xlabel('X gyro')
        cx.set_ylabel('Y gyro')
        cx.set_zlabel('Z gyro')
        message_received = 0;
        print("diff x = "+ str((Xmmaxx + Xmminx)/2)+"with Xzm = "+str(Xmmaxx)+"and Xzmi"+str(Xmminx))
        print("diff y = "+ str((Xmmaxy + Xmminy)/2)+"with Xzm = "+str(Xmmaxy)+"and Xzmi"+str(Xmminy))
        print("diff z = "+ str((Xmmaxz + Xmminz)/2)+"with Xzm = "+str(Xmmaxz)+"and Xzmi"+str(Xmminz))
        plt.show()

        offsetmx = 0.175#float(temp[4])
        offsetmy = +0.055#float(temp[5])
        offsetmz = -0.02#float(temp[6])

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("192.168.4.2", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()