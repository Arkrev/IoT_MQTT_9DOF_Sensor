#include <Wire.h>
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <SparkFunLSM9DS1.h>

const int LED_PIN = 5; // Thing's onboard, green LED


const int selectPins[3] = {4, 13, 12}; // S0~4, S1~13, S2~12
const int zInput = 0;//A0; // Connect common (Z) to A0 (PWM-capable)

const char WiFiAPPSK[] = "sparkfun";

//imu
#define LSM9DS1_M  0x1E // Would be 0x1C if SDO_M is LOW
#define LSM9DS1_AG  0x6B // Would be 0x6A if SDO_AG is LOW
LSM9DS1 imu;

#define DECLINATION 0.68 // Declination (degrees) in Boulder, CO.

//mqtt data
#define mqtt_server "192.168.4.2"
#define data_topic "raw_data"
#define action_topic "button_data"


WiFiServer server(80);
WiFiClient espClient;
PubSubClient client(espClient);

//clock fq variable
long lastMsg = 0;
float temp = 0.0;

//deep sleep variable
const int sleepTimeS = 2;
const int timeBeforeSleep = 15;
long lastMovement;

float roll;
float pitch;
float yaw;
float prevRoll = 0;
float prevPitch = 0;
float prevYaw = 0;

float slept = 0;




//RTC memory var

extern "C" {
#include "user_interface.h" // this is for the RTC memory read/write functions
}

float rtcRoll;
float rtcPitch;
float rtcYaw;
bool notFirstRun;


void shouldIWake() {
  updateImu();
  printAttitude(imu.ax, imu.ay, imu.az,
                -imu.my, -imu.mx, imu.mz);


  float memRoll;
  float memPitch;
  float memYaw;

  system_rtc_mem_read(65, &memRoll, 4);
  system_rtc_mem_read(66, &memPitch, 4);
  system_rtc_mem_read(67, &memYaw, 4);

  if ((roll > memRoll + 2 || roll < memRoll - 2) ||
      (pitch > memPitch + 2 || pitch < memPitch - 2) ||
      (yaw > memYaw + 2 || yaw < memYaw - 2)) {
    lastMovement = millis();

  } else {
    system_rtc_mem_write(65, &roll, 4);
    system_rtc_mem_write(66, &pitch, 4);
    system_rtc_mem_write(67, &yaw, 4);
    ESP.deepSleep(sleepTimeS * 1000);
    delay(100);//useless yet useful

  }
}

void shouldISleep() {

  if ((roll > prevRoll + 2 || roll < prevRoll - 2) ||
      (pitch > prevPitch + 2 || pitch < prevPitch - 2) ||
      (yaw > prevYaw + 2 || yaw < prevYaw - 2)) {
    lastMovement = millis();
    prevRoll = roll;
    prevPitch = pitch;
    prevYaw = yaw;

  } else {
    if ((lastMovement + timeBeforeSleep * 1000) < millis()) {
      system_rtc_mem_write(65, &roll, 4);
      system_rtc_mem_write(66, &pitch, 4);
      system_rtc_mem_write(67, &yaw, 4);
      ESP.deepSleep(sleepTimeS * 1000);
      delay(100);//useless yet useful
    }
  }

}

void setupWiFi()
{


  WiFi.softAPdisconnect();
  WiFi.mode(WIFI_AP);
  int channel = 9;

  // Do a little work to get a unique-ish name. Append the
  // last two bytes of the MAC (HEX'd) to "Thing-":
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  WiFi.softAPmacAddress(mac);
  String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
                 String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
  macID.toUpperCase();
  String AP_NameString = "ESP8266 Thing " + macID + " channel " + channel;

  char AP_NameChar[AP_NameString.length() + 1];
  memset(AP_NameChar, 0, AP_NameString.length() + 1);

  for (int i = 0; i < AP_NameString.length(); i++)
    AP_NameChar[i] = AP_NameString.charAt(i);

  WiFi.softAP(AP_NameChar, WiFiAPPSK, channel, false);
}


void initImu() {

  imu.settings.device.commInterface = IMU_MODE_I2C;
  imu.settings.device.mAddress = LSM9DS1_M;
  imu.settings.device.agAddress = LSM9DS1_AG;

  digitalWrite(LED_PIN, HIGH);
  delay(3000);

  if (!imu.begin())
  {
    digitalWrite(LED_PIN, LOW);
    while (1)
      ;
  }
}



void initMux() {
  for (int i = 0; i < 3; i++)
  {
    pinMode(selectPins[i], OUTPUT);
    digitalWrite(selectPins[i], HIGH);
  }
  pinMode(zInput, INPUT); // Set up Z as an input
}

void initHardware()
{
  Serial.begin(9600);
  initMux();
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  initImu();
}
void reconnect() {
  // Loop until we're reconnected
  byte ledStatus = LOW;
  while (!client.connected()) {
    // Attempt to connect
 

    digitalWrite(LED_PIN, ledStatus);
    ledStatus = (ledStatus == HIGH) ? LOW : HIGH;
    delay(100);
    if (client.connect("ESP8266Client")) {
      //if (client.connect("ESP8266Client", mqtt_user, mqtt_password)) {
    } else {
      // Wait 1 second before retrying
      delay(1000);
    }
  }
}



void updateImu() {
  if ( imu.gyroAvailable() )
  {
    // To read from the gyroscope,  first call the
    // readGyro() function. When it exits, it'll update the
    // gx, gy, and gz variables with the most current data.
    imu.readGyro();
  }
  if ( imu.accelAvailable() )
  {
    // To read from the accelerometer, first call the
    // readAccel() function. When it exits, it'll update the
    // ax, ay, and az variables with the most current data.
    imu.readAccel();
  }
  if ( imu.magAvailable() )
  {
    // To read from the magnetometer, first call the
    // readMag() function. When it exits, it'll update the
    // mx, my, and mz variables with the most current data.
    imu.readMag();
  }

}





// The selectMuxPin function sets the S0, S1, and S2 pins
// accordingly, given a pin from 0-7.
void selectMuxPin(byte pin)
{
  for (int i = 0; i < 3; i++)
  {
    if (pin & (1 << i))
      digitalWrite(selectPins[i], HIGH);
    else
      digitalWrite(selectPins[i], LOW);
  }
}



//use roll, pitch and yaw, which cause gimbal lock. //used for waking up from deep sleep.
void printAttitude(float ax, float ay, float az, float mx, float my, float mz)
{


  roll = atan2(ay, az);
  pitch = atan2(-ax, sqrt(ay * ay + az * az));
  if (my == 0)
    yaw = (mx < 0) ? PI : 0;
  else
    yaw = atan2(mx, my);

  yaw -= DECLINATION * PI / 180;

  if (yaw > PI) yaw -= (2 * PI);
  else if (yaw < -PI) yaw += (2 * PI);
  else if (yaw < 0) yaw += 2 * PI;

  // Convert everything from radians to degrees:
  yaw *= 180.0 / PI;
  pitch *= 180.0 / PI;
  roll  *= 180.0 / PI;

}

//cycle through 
void publish_button_action(){

  // Loop through all eight pins.
  String message = "button_action ";
  for (byte pin=0; pin<=7; pin++)
  {
    selectMuxPin(pin); // Select one at a time
    int inputValue = digitalRead(zInput); // and read Z
    message.concat(String(inputValue)+ " ");
  }
  client.publish(action_topic, message.c_str(), false);
}


void publish_raw_data() {
  String message = "rd ";
  message.concat(imu.ax);
  message.concat(" ");
  message.concat(imu.ay);
  message.concat(" ");
  message.concat(imu.az);
  message.concat(" ");
  message.concat(imu.mx);
  message.concat(" ");
  message.concat(imu.my);
  message.concat(" ");
  message.concat(imu.mz);
  message.concat(" ");
  message.concat(imu.gx);
  message.concat(" ");
  message.concat(imu.gy);
  message.concat(" ");
  message.concat(imu.gz);/*
  message.concat(" ed ");
  message.concat(roll);
  message.concat(" ");
  message.concat(pitch);
  message.concat(" ");
  message.concat(yaw);*/
  message.concat(" ba ");
  for (byte pin=0; pin<=7; pin++)
  {
    selectMuxPin(pin); // Select one at a time
    int inputValue = digitalRead(zInput); // and read Z
    message.concat(String(inputValue)+ " ");
  } 
  client.publish(data_topic, message.c_str(), false);
}


void setup() {


  initHardware();
  system_rtc_mem_read(64, &notFirstRun, 4);
  if (notFirstRun) {
    slept = slept+1;
    shouldIWake();
  } else {

    notFirstRun = true;
    system_rtc_mem_write(64, &notFirstRun, 4);
  }

  setupWiFi();
  server.begin();
  client.setServer(mqtt_server, 1883);



}

void loop() {
  if (!client.connected()) {
    //Serial.println("reco");
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > 10) {
    lastMsg = now;
    //sensor part:

    //publish_button_action();
    updateImu();
    printAttitude(imu.ax, imu.ay, imu.az, -imu.my, -imu.mx, imu.mz);
    publish_raw_data();
    shouldISleep();

  }
}
