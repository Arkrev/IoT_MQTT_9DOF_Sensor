#include <Wire.h>
#include <SPI.h>
#include <SparkFunLSM9DS1.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

WiFiServer server(80);

// SDO_XM and SDO_G are both pulled high, so our addresses are:
#define LSM9DS1_M  0x1E // Would be 0x1C if SDO_M is LOW
#define LSM9DS1_AG  0x6B // Would be 0x6A if SDO_AG is LOW
////////////////////////////
// Sketch Output Settings //
////////////////////////////

// http://www.ngdc.noaa.gov/geomag-web/#declination
#define DECLINATION 0.68 // Declination (degrees) in Boulder, CO.

#define mqtt_server "192.168.4.2"//"192.168.1.159"
#define mqtt_user "julien"
#define mqtt_password "isen"


#define gyro_x_topic "gyro_x"
#define gyro_y_topic "gyro_y"
#define gyro_z_topic "gyro_z"

#define accel_x_topic "accel_x"
#define accel_y_topic "accel_y"
#define accel_z_topic "accel_z"

#define mag_x_topic "mag_x"
#define mag_y_topic "mag_y"
#define mag_z_topic "mag_z"

#define roll_topic "roll"
#define pitch_topic "pitch"
#define yaw_topic "yaw"

LSM9DS1 imu;
WiFiClient espClient;
PubSubClient client(espClient);


//pin list
const int LED_PIN = 5;
const int ANALOG_PIN = A0;
const int DIGITAL_PIN = 12;

const char WiFiSSID[] = "VisualDimensionBis";//"LinksysVSRepeater";
const char WiFiPSK[] = "4V8rle62";


long lastMsg = 0;
float temp = 0.0;



void setup() {
  initHardware();
  connectWiFi();
  client.setServer(mqtt_server, 1883);
  digitalWrite(LED_PIN, HIGH);
  initSerial();
  
}
void initHardware(){
  pinMode(DIGITAL_PIN,INPUT_PULLUP);
  pinMode(LED_PIN,OUTPUT);
  digitalWrite(LED_PIN,LOW);
  initImu();
  

  
  
}
void initSerial(){
  
  Serial.begin(9600);
  
}


void initImu(){
  
  imu.settings.device.commInterface = IMU_MODE_I2C;
  imu.settings.device.mAddress = LSM9DS1_M;
  imu.settings.device.agAddress = LSM9DS1_AG;
  
  digitalWrite(LED_PIN, HIGH);
  delay(3000);

  if (!imu.begin())
  {
    digitalWrite(LED_PIN, LOW);
    while (1)
      ;
  }
}

void connectWiFi()
{
  byte ledStatus = LOW;

  // Set WiFi mode to station (as opposed to AP or AP_STA)
  //WiFi.mode(WIFI_STA);
  WiFi.mode(WIFI_AP);

  // WiFI.begin([ssid], [passkey]) initiates a WiFI connection
  // to the stated [ssid], using the [passkey] as a WPA, WPA2,
  // or WEP passphrase.
  //WiFi.begin(WiFiSSID, WiFiPSK);
  WiFi.softAP(WiFiSSID, WiFiPSK);

  // Use the WiFi.status() function to check if the ESP8266
  // is connected to a WiFi network.
  /*while (WiFi.status() != WL_CONNECTED)
  {
    // Blink the LED
    digitalWrite(LED_PIN, ledStatus); // Write LED high/low
    ledStatus = (ledStatus == HIGH) ? LOW : HIGH;

    // Delays allow the ESP8266 to perform critical tasks
    // defined outside of the sketch. These tasks include
    // setting up, and maintaining, a WiFi connection.
    delay(100);
    // Potentially infinite loops are generally dangerous.
    // Add delays -- allowing the processor to perform other
    // tasks -- wherever possible.
  }*/
}

void reconnect() {
  // Loop until we're reconnected
  byte ledStatus = LOW;
  while (!client.connected()) {
    // Attempt to connect
    // If you do not want to use a username and password, change next line to

    digitalWrite(LED_PIN,ledStatus);
    ledStatus = (ledStatus == HIGH)? LOW:HIGH;
    delay(100);
    if (client.connect("ESP8266Client")) {
    //if (client.connect("ESP8266Client", mqtt_user, mqtt_password)) {
      //client.subscribe("homeassistant/switch1");
      //will be needed for deep sleep ?
    } else {
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


void printGyro()
{
  client.publish(gyro_x_topic, String(imu.calcGyro(imu.gx)).c_str(), false);
  client.publish(gyro_y_topic, String(imu.calcGyro(imu.gy)).c_str(), false);
  client.publish(gyro_z_topic, String(imu.calcGyro(imu.gz)).c_str(), false);
}

void printAccel(){
  client.publish(accel_x_topic, String(imu.calcAccel(imu.ax)).c_str(), false);
  client.publish(accel_y_topic, String(imu.calcAccel(imu.ay)).c_str(), false);
  client.publish(accel_z_topic, String(imu.calcAccel(imu.az)).c_str(), false);
}
void printMag(){
  client.publish(mag_x_topic, String(imu.calcMag(imu.mx)).c_str(), false);
  client.publish(mag_y_topic, String(imu.calcMag(imu.my)).c_str(), false);
  client.publish(mag_z_topic, String(imu.calcMag(imu.mz)).c_str(), false);
}


// Calculate pitch, roll, and heading.
// Pitch/roll calculations take from this app note:
// http://cache.freescale.com/files/sensors/doc/app_note/AN3461.pdf?fpsp=1
// Heading calculations taken from this app note:
// http://www51.honeywell.com/aero/common/documents/myaerospacecatalog-documents/Defense_Brochures-documents/Magnetic__Literature_Application_notes-documents/AN203_Compass_Heading_Using_Magnetometers.pdf
void printAttitude(float ax, float ay, float az, float mx, float my, float mz)
{
  float roll = atan2(ay, az);
  float pitch = atan2(-ax, sqrt(ay * ay + az * az));
  
  float heading;
  if (my == 0)
    heading = (mx < 0) ? PI : 0;
  else
    heading = atan2(mx, my);
    
  heading -= DECLINATION * PI / 180;
  
  if (heading > PI) heading -= (2 * PI);
  else if (heading < -PI) heading += (2 * PI);
  else if (heading < 0) heading += 2 * PI;
  
  // Convert everything from radians to degrees:
  heading *= 180.0 / PI;
  pitch *= 180.0 / PI;
  roll  *= 180.0 / PI;

  Serial.println("sr");
  client.publish(roll_topic, String(roll).c_str(), false);
  Serial.println("sp");
  client.publish(pitch_topic, String(pitch).c_str(), false);
  Serial.println("sy");
  client.publish(yaw_topic, String(heading).c_str(), false);
  /*
  Serial.print("roll ");
  Serial.println(String(roll).c_str());
  Serial.print("pitch ");
  Serial.println(String(pitch).c_str());
  Serial.print("yaw ");
  Serial.println(String(heading).c_str());
  */
}



void loop() {/*
  if (!client.connected()) {
    Serial.println("reco");
    reconnect();
  }
  client.loop();*/
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  long now = millis();
  digitalWrite(LED_PIN,LOW);
  if (now - lastMsg > 10) {
    lastMsg = now;
    digitalWrite(LED_PIN,HIGH);

    
    

    //sensor part:
    // Update the sensor values whenever new data is available
    if ( imu.gyroAvailable() )
    {
      // To read from the gyroscope,  first call the
      // readGyro() function. When it exits, it'll update the
      // gx, gy, and gz variables with the most current data.
      imu.readGyro();
    }
    if ( imu.accelAvailable() )
    {
      // To read from the accelerometer, first call the
      // readAccel() function. When it exits, it'll update the
      // ax, ay, and az variables with the most current data.
      imu.readAccel();
    }
    if ( imu.magAvailable() )
    {
      // To read from the magnetometer, first call the
      // readMag() function. When it exits, it'll update the
      // mx, my, and mz variables with the most current data.
      imu.readMag();
    }
    
    printAttitude(imu.ax, imu.ay, imu.az, 
                 -imu.my, -imu.mx, imu.mz);

    
    //printGyro();
    //printAccel();
    //printMag();

  }
}



